# Test cases for algorithms
from typing import Dict

test: Dict = {
    "sphere": {
        "HC": {
            "dimensions": 2,
            "lower_bound": -100,
            "upper_bound": 100,
            "max_iterations": 100,
            "bandwidth": 2.5,
            "function": "sphere",
        },
        "HC_RR": {
            "dimensions": 2,
            "lower_bound": -100,
            "upper_bound": 100,
            "max_iterations": 100,
            "bandwidth": 2.5,
            "time": 10,
            "function": "sphere",
        },
    },
    "step": {
        "HC": {
            "dimensions": 2,
            "lower_bound": -100,
            "upper_bound": 100,
            "max_iterations": 100,
            "bandwidth": 2.5,
            "function": "step",
        },
        "HC_RR": {
            "dimensions": 2,
            "lower_bound": -100,
            "upper_bound": 100,
            "max_iterations": 100,
            "bandwidth": 2.5,
            "time": 10,
            "function": "step",
        },
    },
    "schwefel": {
        "HC": {
            "dimensions": 2,
            "lower_bound": -100,
            "upper_bound": 100,
            "max_iterations": 100,
            "bandwidth": 2.5,
            "function": "schwefel",
        },
        "HC_RR": {
            "dimensions": 2,
            "lower_bound": -100,
            "upper_bound": 100,
            "max_iterations": 100,
            "bandwidth": 2.5,
            "time": 10,
            "function": "schwefel",
        },
    },
    "rastrigin": {
        "HC": {
            "dimensions": 2,
            "lower_bound": -5.12,
            "upper_bound": 5.12,
            "max_iterations": 100,
            "bandwidth": 2.5,
            "function": "rastrigin",
        },
        "HC_RR": {
            "dimensions": 2,
            "lower_bound": -5.12,
            "upper_bound": 5.12,
            "max_iterations": 100,
            "bandwidth": 2.5,
            "time": 10,
            "function": "rastrigin",
        },
    },
    "griewank": {
        "HC": {
            "dimensions": 2,
            "lower_bound": -600,
            "upper_bound": 600,
            "max_iterations": 100,
            "bandwidth": 2.5,
            "function": "griewank",
        },
        "HC_RR": {
            "dimensions": 2,
            "lower_bound": -600,
            "upper_bound": 600,
            "max_iterations": 100,
            "bandwidth": 2.5,
            "time": 10,
            "function": "griewank",
        },
    },
    "ackley": {
        "HC": {
            "dimensions": 2,
            "lower_bound": -32,
            "upper_bound": 32,
            "max_iterations": 100,
            "bandwidth": 2.5,
            "function": "ackley",
        },
        "HC_RR": {
            "dimensions": 2,
            "lower_bound": -32,
            "upper_bound": 32,
            "max_iterations": 100,
            "bandwidth": 2.5,
            "time": 10,
            "function": "ackley",
        },
    },
}
