import math
from typing import Dict

import numpy as np


def sphere(cells):
    """
    #Sphere
    d = len(cells)
    result = 0
    for  i in range(0, d):
      result = result + np.power(cells[i], 2)
    """
    result = (cells * cells).sum()
    return result


def ackley(cells):
    # Ackley
    d = len(cells)
    t1 = -20 * math.exp(-0.2 * math.sqrt((1 / d) * (np.power(cells, 2)).sum()))
    t2 = math.exp((1 / d) * (np.cos(2 * np.pi * cells)).sum())
    return t1 - t2 + 20 + math.exp(1)


def step(cells):
    # Step function
    cells = np.array(list(map(lambda x: int(x + 0.5), cells)))
    result = (cells * cells).sum()
    return result


def rastrigin(cells):
    # Rastrigin
    d = len(cells)
    t1 = (np.power(cells, 2)).sum()
    t2 = 10 * (np.cos(2 * np.pi * cells)).sum()
    return 10 * d + t1 - t2


def griewank(cells):
    # Griewank
    d = len(cells)
    t1 = (np.power(cells, 2)).sum() / 4000
    t2 = 1
    for i in range(d):
        t2 *= np.cos(cells[i] / math.sqrt(i + 1))
    return t1 - t2 + 1


def schwefel(cells):
    # Schwefel
    d = len(cells)
    t1 = 0
    for i in range(d):
        t1 += cells[i] * math.sin(math.sqrt(abs(cells[i])))
    return 418.9829 * d - t1


functions: Dict = {
    "sphere": sphere,
    "ackley": ackley,
    "step": step,
    "schwefel": schwefel,
    "rastrigin": rastrigin,
    "griewank": griewank,
    "ackley": ackley,
}


def evaluate(cells: np.ndarray, function: str = "sphere"):
    return functions[function](cells)
