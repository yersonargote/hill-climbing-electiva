import numpy as np

from functions import evaluate


def randomInitialization(dimension: int, lower_bound: int, upper_bound: int):
    cells = np.random.uniform(low=lower_bound, high=upper_bound, size=(dimension,))
    fitness = evaluate(cells)
    return cells, fitness


def uniform_tweak(
    cells: np.ndarray,
    lower_bound: int,
    upper_bound: int,
    bandwidth: float,
    function="sphere",
):
    bandwidths = np.random.uniform(low=-bandwidth, high=bandwidth, size=(cells.size,))
    cells = cells + bandwidths
    cells[cells < lower_bound] = lower_bound
    cells[cells > upper_bound] = upper_bound
    fitness = evaluate(cells, function)
    return cells, fitness
