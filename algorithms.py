from test import test
from typing import Dict

import numpy as np

from utils import randomInitialization, uniform_tweak


def evolve(algorithm: str, kwargs: Dict):
    """Evolve function take algorithm and kwargs and call the algorithm"""
    evolve = algos[algorithm]
    return evolve(**kwargs)


def hill_climbing(
    dimensions: int,
    lower_bound: int,
    upper_bound: int,
    max_iterations: int,
    bandwidth: float,
    function: str = "sphere",
):
    x = np.zeros(max_iterations, int)
    y = np.zeros(max_iterations, float)

    S, fS = randomInitialization(
        dimensions, lower_bound, upper_bound
    )  # ojo NO hace S = best
    x[0] = 0
    y[0] = fS

    for iteration in range(1, max_iterations):
        R, fR = uniform_tweak(S, lower_bound, upper_bound, bandwidth, function)
        if fR < fS:
            S = np.copy(R)
            fS = fR
        x[iteration] = iteration
        y[iteration] = fS
    return x, y


def hill_climbing_with_random_restarts(
    dimensions: int,
    lower_bound: int,
    upper_bound: int,
    max_iterations: int,
    bandwidth: float,
    time: int,
    function="sphere",
):
    x = np.zeros(max_iterations, int)
    y = np.zeros(max_iterations, float)

    S, fS = randomInitialization(
        dimensions, lower_bound, upper_bound
    )  # ojo NO hace S = best
    x[0] = 0
    y[0] = fS

    B, fB = S, fS
    for iteration in range(1, max_iterations):
        rg = int(np.random.uniform(low=0, high=time, size=1))
        for _ in range(rg):
            R, fR = uniform_tweak(S, lower_bound, upper_bound, bandwidth, function)
            if fR < fS:
                S = np.copy(R)
                fS = fR
            x[iteration] = iteration
            y[iteration] = fS
            iteration += 1
            if iteration >= max_iterations:
                break
        if fB < fS:
            S = np.copy(B)
            fS = fB
    return x, y


algos: Dict = {"HC": hill_climbing, "HC_RR": hill_climbing_with_random_restarts}
