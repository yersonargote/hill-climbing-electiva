from test import test

import matplotlib.pyplot as plt

from algorithms import evolve

if __name__ == "__main__":
    solutions = {}
    for function, algos in test.items():
        solutions[function] = {}
        for algo in algos:
            x, y = evolve(algo, test[function][algo])
            solutions[function][algo] = (x, y)

    figure, axis = plt.subplots(2, 3, figsize=(16, 9))
    
    print("Function        HC              HCRR")

    for i in range(6):
        function = list(solutions.keys())[i]
        axis[i // 3, i % 3].set_title(function)
        print(f"{function}{(16-len(function))*' '}", end="")
        axis[i // 3, i % 3].set_xlabel("Iteration")
        axis[i // 3, i % 3].set_ylabel("Fitness")
        for algo in solutions[function]:
            x, y = solutions[function][algo]
            axis[i // 3, i % 3].plot(x, y, label=algo)
            axis[i // 3, i % 3].set_title(function)
            axis[i // 3, i % 3].legend()
            print(f"{round(y[-1], 10)}{(16-len(str(round(y[-1], 10))))*' '}", end="")
        print()
    plt.show()
